//
//  GameScene.swift
//  Firework
//
//  Created by Hariharan S on 21/06/24.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    var gameTimer: Timer?
    var fireworks = [SKNode]()

    let leftEdge = -22
    let bottomEdge = -22
    let rightEdge = 1024 + 22
    
    var scoreLabel: SKLabelNode!
    var score = 0 {
        didSet {
            self.scoreLabel.text = "Score: \(self.score)"
        }
    }
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "background")
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .replace
        background.zPosition = -1
        self.addChild(background)
        
        self.scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        self.scoreLabel.position = CGPoint(
            x: 16,
            y: 16
        )
        self.scoreLabel.horizontalAlignmentMode = .left
        self.addChild(self.scoreLabel)
        self.score = 0

        self.gameTimer = Timer.scheduledTimer(
            timeInterval: 6,
            target: self,
            selector: #selector(self.launchFireworks),
            userInfo: nil,
            repeats: true
        )
    }
    
    override func touchesBegan(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        super.touchesBegan(touches, with: event)
        self.checkTouches(touches)
    }

    override func touchesMoved(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        super.touchesMoved(touches, with: event)
        self.checkTouches(touches)
    }
    
    override func update(_ currentTime: TimeInterval) {
        for (index, firework) in self.fireworks.enumerated().reversed() {
            if firework.position.y > 900 {
                self.fireworks.remove(at: index)
                firework.removeFromParent()
            }
        }
    }
    
    func explodeFireworks() {
        var numExploded = 0
        for (index, fireworkContainer) in self.fireworks.enumerated().reversed() {
            guard let firework = fireworkContainer.children.first as? SKSpriteNode
            else {
                continue
            }

            if firework.name == "selected" {
                // destroy this firework!
                self.explode(firework: fireworkContainer)
                self.fireworks.remove(at: index)
                numExploded += 1
            }
        }

        switch numExploded {
        case 0:
            // nothing – rubbish!
            break
        case 1:
            self.score += 200
        case 2:
            self.score += 500
        case 3:
            self.score += 1500
        case 4:
            self.score += 2500
        default:
            self.score += 4000
        }
    }
}

// MARK: - Private Helpers

private extension GameScene {
    func checkTouches(_ touches: Set<UITouch>) {
        guard let touch = touches.first 
        else {
            return
        }

        let location = touch.location(in: self)
        let nodesAtPoint = nodes(at: location)

        for case let nod as SKSpriteNode in nodesAtPoint {
            guard nod.name == "firework"
            else {
                continue
            }
            
            for parent in self.fireworks {
                guard let firework = parent.children.first as? SKSpriteNode
                else { 
                    continue
                }

                if firework.name == "selected" && firework.color != nod.color {
                    firework.name = "firework"
                    firework.colorBlendFactor = 1
                }
            }
            
            nod.name = "selected"
            nod.colorBlendFactor = 0
        }
    }
    
    func createFirework(xMovement: CGFloat, x: Int, y: Int) {
        // 1
        let node = SKNode()
        node.position = CGPoint(x: x, y: y)
        
        // 2
        let firework = SKSpriteNode(imageNamed: "rocket")
        firework.colorBlendFactor = 1
        firework.name = "firework"
        node.addChild(firework)
        
        // 3
        switch Int.random(in: 0...2) {
        case 0:
            firework.color = .cyan
        case 1:
            firework.color = .green
        case 2:
            firework.color = .red
        default:
            break
        }
        
        let path = UIBezierPath()
        path.move(to: .zero)
        path.addLine(to: CGPoint(x: xMovement, y: 1000))
        
        // 5
        let move = SKAction.follow(
            path.cgPath,
            asOffset: true,
            orientToPath: true,
            speed: 200
        )
        node.run(move)
        
        // 6
        if let emitter = SKEmitterNode(fileNamed: "fuse") {
            emitter.position = CGPoint(x: 0, y: -22)
            node.addChild(emitter)
        }
        
        // 7
        self.fireworks.append(node)
        self.addChild(node)
    }
    
    func explode(firework: SKNode) {
        if let emitter = SKEmitterNode(fileNamed: "explode") {
            emitter.position = firework.position
            addChild(emitter)
        }
        firework.removeFromParent()
    }
    
    @objc func launchFireworks() {
        let movementAmount: CGFloat = 1800
        switch Int.random(in: 0...3) {
        case 0:
            // fire five, straight up
            self.createFirework(xMovement: 0, x: 512, y: bottomEdge)
            self.createFirework(xMovement: 0, x: 512 - 200, y: bottomEdge)
            self.createFirework(xMovement: 0, x: 512 - 100, y: bottomEdge)
            self.createFirework(xMovement: 0, x: 512 + 100, y: bottomEdge)
            self.createFirework(xMovement: 0, x: 512 + 200, y: bottomEdge)

        case 1:
            // fire five, in a fan
            self.createFirework(xMovement: 0, x: 512, y: bottomEdge)
            self.createFirework(xMovement: -200, x: 512 - 200, y: bottomEdge)
            self.createFirework(xMovement: -100, x: 512 - 100, y: bottomEdge)
            self.createFirework(xMovement: 100, x: 512 + 100, y: bottomEdge)
            self.createFirework(xMovement: 200, x: 512 + 200, y: bottomEdge)

        case 2:
            // fire five, from the left to the right
            self.createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 400)
            self.createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 300)
            self.createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 200)
            self.createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 100)
            self.createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge)

        case 3:
            // fire five, from the right to the left
            self.createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 400)
            self.createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 300)
            self.createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 200)
            self.createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 100)
            self.createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge)

        default:
            break
        }
    }
}

